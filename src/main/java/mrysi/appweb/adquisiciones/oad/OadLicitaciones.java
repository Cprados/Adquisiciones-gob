/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.adquisiciones.oad;

import java.util.List;
import mrysi.appweb.adquisiciones.entity.Licitacion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Gustavo
 */
public interface OadLicitaciones extends JpaRepository<Licitacion, String> {
    //idLicitacion
    Licitacion findByidLicitacion(int idLicitacion);
    
}
